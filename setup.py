from setuptools import setup, find_packages
setup(
    name="chat",
    version="0.1",
    packages=find_packages(),
    install_requires=[],

    package_data={
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
    },
    entry_points={
        'console_scripts': [
            'chat=chat:runcli'
        ]
    },

    # metadata for upload to PyPI
    author="Adrien Sohier",
    author_email="adrien.sohier@art-software.fr",
    description="A CLI Curses chat system",
    license="GPLv3",
    keywords="chat curses",
    # url="",   # project home page, if any

    # could also include long_description, download_url, classifiers, etc.
)
