#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""CursesChat data management
This is the core of CursesChat."""

import os
import time

class Data:
    """Chat log data"""
    def __init__(self, users=[('Adrien', 0b1110)]):
        self.users = users
        self.curUserId = 0
        self.lastUserId = 0
        self.log = {"main":[]}
        self.room = 0
        self.rooms = ["main"]

    def addUser(self, name, color):
        """Adds a user"""
        self.users.append((name, color))

    def nextUser(self):
        """Switch to next user"""
        self.curUserId = (self.curUserId+1) % len(self.users)
        while self.users[self.curUserId][0][0] + self.users[self.curUserId][0][-1] == '()' and self.users[self.curUserId][1] == 240:
            self.curUserId = (self.curUserId+1) % len(self.users)

    def prevUser(self):
        """Switch to previous user"""
        self.curUserId = (self.curUserId-1) % len(self.users)
        while self.users[self.curUserId][0][0] + self.users[self.curUserId][0][-1] == '()' and self.users[self.curUserId][1] == 240:
            self.curUserId = (self.curUserId-1) % len(self.users)

    def curUser(self):
        """Get current user name"""
        return self.users[self.curUserId][0]

    def getLogOffset(self, offset, lines, room=""):
        """Get a certain amount of lines from the log, with an initial offset"""
        if room=="":
            room=self.rooms[self.room]

        if not room in self.log:
            self.log[room] = []
            self.rooms.append(room)
        length = len(self.log[room])

        lineList = []

        if lines>length:
            lineList += [['', 0, '']] * (lines - length)   # Padding on the top if the log is too small (no offset accounted)

        if room in self.log:
            if lines+offset>length:
                lineList += self.log[room][max(0,length-lines):length]
            else:
                lineList += self.log[room][offset:offset+lines]

        return lineList

    def getLogSize(self, room=""):
        """Get log size"""
        if room=="":
            room=self.rooms[self.room]
        
        if not room in self.log:
            self.log[room] = []
            self.rooms.append(room)
        
        return len(self.log[room])

    def nextRoom(self):
        """Switches to the next available room"""
        self.room = (self.room + 1) % len(self.rooms)

    def prevRoom(self):
        """Switches to the previous available room"""
        self.room = (self.room - 1) % len(self.rooms)

    def appendLog(self, text, room=""):
        """Append data to log"""
        
        if room=="":
            room=self.rooms[self.room]

        # Creates the room if it's a brand new one
        if not room in self.log:
            self.log[room] = []
            self.rooms.append(room)

        self.log[room].append([time.strftime("[%F %T]"), self.curUserId, text])
    
    def exportLog(self, filePath, room=""):
        """Export a room's log to text file"""
        logFile = open(filePath, "w")
        
        if room=="":
            room=self.rooms[self.room]
        
        # Create room if new
        if not room in self.log:
            self.log[room] = []
            self.rooms.append(room)
        
        for logEntry in self.log[room]:
            userName, colorId = self.users[logEntry[1]]
            logFile.write("%s <\x1B[38;5;%dm%s\x1B[0m> %s\n" % (logEntry[0], colorId, userName, logEntry[2]))
        
        logFile.close()

    def exportAll(self, rootPath="."):
        """Export every room log to a file under rootPath.
        RootPath must exist."""
        if not os.path.isdir(rootPath+"/"):
            print("ERR: {arg1} is not a valid directory".format(arg1=rootPath))
            return
        
        for room in self.log.keys():
            print("\x1B[1;34m:: \x1B[37m{chan}\x1B[0m exported to \x1B[1m{root}/{chan}.log\x1B[0m".format(chan=room, root=rootPath))
            self.exportLog("{root}/{chan}.log".format(root=rootPath, chan=room), room)

    def importLog(self, filePath, room=""):
        """Import a text log into a new room"""
        
        if room=="":
            room=self.rooms[self.room]

        if not room in self.log:
            self.log[room] = []

        userDict = {name[0]:idx for idx, name in enumerate(self.users)}
        if not os.path.isfile(filePath):
            return

        logFile = open(filePath, "r")

        for logLine in logFile.readlines():
            try:
                # If something goes wrong with this line, jump to the next one
                lineTimestamp = " ".join(logLine.split()[0:2])

                userTag = logLine[len(lineTimestamp)+2:].split(">")[0]

                # Get color from ansi escaped control color if present (else make it to default grey)
                if userTag.find("\x1B")>=0:
                    userTag = userTag.split("\x1B[")[1]
                    userColor, userName = userTag[0:userTag.find("m")], userTag[userTag.find("m")+1:]
                else:
                    userColor = "37"
                    userName = userTag

                if not userName in userDict:
                    # New user, get his / her color id first
                    userColorId = 0
                    
                    # Light color 1;x or 9x
                    if userColor[0:2]=="1;" or (len(userColor)==1 and userColor=="1"):
                        if len(userColor)>1:
                            userColor = userColor[2:]
                        else:
                            userColor = "37"
                        userColorId = 0b1000
                    if userColor[0:1]=="9":
                        userColorId = 0b1000

                    # 256color-palette color id ?
                    if userColor[0:5]=="38;5;":
                        userColorId = int(userColor[5:])
                    else:
                        # Normal color
                        userColorId += int(userColor[1])

                    # Add user to lookup table 
                    userDict[userName] = len(self.users)
                    self.users.append((userName, userColorId))

                # Add (finally!!) log line
                self.log[room].append([lineTimestamp, userDict[userName], logLine[logLine.find(">")+2:].replace("\n", "")])
            except:
                pass

        logFile.close()

# vim: ts=4 sts=4 sw=4 et
