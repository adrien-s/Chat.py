#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""CursesChat Window management"""

import curses
import os
import time

class Win:
    def __init__(self, chatData):
        """Initiates curses"""

        self.data = chatData

        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        self.screen.keypad(1)

        self.height, self.width = self.screen.getmaxyx()
        
        self.offset = max(0,self.data.getLogSize() - (self.height-4))
        self.hOffset = 0

        self.win = curses.newwin(self.height-2, self.width, 1, 0)
        self.__initWinLayouts__()
        
        curses.start_color()
        curses.use_default_colors()

        # 'normal' xterm is already 256-color capable
        if os.environ["TERM"].find("xterm") >= 0 or os.environ["TERM"].find("screen") >= 0:
            for i in range(0, 256):
                curses.init_pair(i, i, -1)
        else:
            for i in range(0, curses.COLORS):
                curses.init_pair(i , i, -1)

    def __resizeTerm__(self):
        """Resizes the terminal"""
        self.height, self.width = self.screen.getmaxyx()
        curses.resizeterm(self.height, self.width)

        self.win = curses.newwin(self.height-2, self.width, 1, 0)
        self.offset = max(0,self.data.getLogSize() - (self.height-4))
    
        self.win.clear()
        self.screen.clear()

        self.__initWinLayouts__()
        
    def __initWinLayouts__(self):
        """Draw window borders"""
        self.win.clear()

        self.win.border(0)
        self.screen.refresh()
        self.win.refresh()

    def __clear__(self):
        """Clear the terminal and stop everything Curses-related"""
        curses.nocbreak()
        self.screen.keypad(0)
        curses.echo()
        curses.endwin()

    def drawWindow(self):
        """Draw the windows"""
        if curses.is_term_resized(self.height, self.width):
            self.__resizeTerm__()

        y = 0
        line = 0
        userw = max([len(i[0]) for i in self.data.users])
        lineTextSize = len(str(self.data.getLogSize()))
        maxLine = self.width - (lineTextSize+userw+25)

        logSize = self.data.getLogSize()

        if self.offset+self.height-4>logSize:
            self.offset = max(0, logSize - (self.height-4))

        self.screen.addstr(0, 0, "│", curses.color_pair(0b0111))

        for roomId, room in enumerate(self.data.rooms):
            self.screen.addstr(" %d:%s " % (roomId, room), curses.color_pair(0b0111 + 0x400*(room==self.data.rooms[self.data.room])))
            self.screen.addch("│", curses.color_pair(0b0111))
        self.screen.clrtoeol()

        if self.height-4>logSize:
            self.win.clear()

        lastUserName = ""

        for lineLog in self.data.getLogOffset(self.offset, self.height-4):
            if lineLog[0]=="":
                self.win.addstr(1+y, 1, "~", curses.color_pair(0b1100))
            else:
                # line number
                self.win.addstr(1+y, 1, "%*s: " % (lineTextSize, line+self.offset), curses.color_pair(0b1011))
                userName = self.data.users[lineLog[1]][0]
                
                today = (lineLog[0][1:11]==time.strftime("%F"))
                # Timestamp
                if userName != lastUserName:
                    self.win.addstr("{}/{} ".format(lineLog[0][9:11], lineLog[0][6:8]), curses.color_pair(0b010))
                    self.win.addstr("[{}".format(lineLog[0][12:17]), curses.color_pair(0b1010))
                    self.win.addstr("{}".format(lineLog[0][17:20], lineLog[0][6:8], lineLog[0][12:20]), curses.color_pair(0b010))
                    self.win.addstr("]", curses.color_pair(0b1010))
                else:
                    self.win.addstr(" "*16, curses.color_pair(0))

                # Nickname
                if userName == lastUserName:
                    self.win.addstr(" %*s" % (userw, " "), curses.color_pair(self.data.users[lineLog[1]][1]))
                else:
                    self.win.addstr(" %*s" % (userw, self.data.users[lineLog[1]][0]), curses.color_pair(self.data.users[lineLog[1]][1]))
                    lastUserName = userName
                # Text
                self.win.addstr(' │ ', curses.color_pair(0b010))
                self.win.addstr(lineLog[2][self.hOffset:min(self.hOffset+maxLine+8*(today),len(lineLog[2]))])
                self.win.clrtoeol()
                line += 1
            y += 1
        self.win.border(0)
        self.drawInputWin()
        self.win.refresh()

    def drawInputWin(self):
        """Draw the input window"""
        self.screen.addstr(self.height-1, 0, "<" + str(self.data.curUserId)+":")
        self.screen.addstr(self.data.curUser(), curses.color_pair(self.data.users[self.data.curUserId][1]))
        self.screen.addstr("> ")
        self.screen.clrtoeol()
        self.screen.refresh()

    def showHelp(self):
        """Show help window"""
        helpW = curses.newwin(self.height-2, self.width, 1, 0)
        helpW.addstr(1, 1, "[num] G       : offset (default: last line)", curses.color_pair(0b0111))
        helpW.addstr(2, 1, "[num] d       : delete log line (default: last line)", curses.color_pair(0b0111))
        helpW.addstr(3, 1, "⇑, ⇓, ⇐, ⇒    : scroll", curses.color_pair(0b0111))
        helpW.addstr(4, 1, "[num] i,Enter : insert line and optionally set user", curses.color_pair(0b0111))
        helpW.addstr(5, 1, "n, p, +, -    : set user", curses.color_pair(0b0111))
        helpW.addstr(6, 1, "[num] TAB     : switch to next room (or given room ID)", curses.color_pair(0b0111))
        helpW.addstr(7, 1, "Shift TAB     : switch to previous room", curses.color_pair(0b0111))
        helpW.addstr(8, 1, "r             : creates a new room", curses.color_pair(0b0111))
        helpW.addstr(9, 1, "D             : deletes current room", curses.color_pair(0b0111))
        helpW.addstr(10, 1, "c             : removes current room from active list (DON'T delete it)", curses.color_pair(0b0111))
        helpW.addstr(11, 1, "l             : list rooms", curses.color_pair(0b0111))
        helpW.addstr(12, 1, "e / w         : Edit or Write current room log from / to a text log file", curses.color_pair(0b0111))
        helpW.addstr(13, 1, "q             : exit", curses.color_pair(0b0111))
        helpW.border(0)
        helpW.refresh()
        
        helpW.get_wch() # Wait a key press
        del helpW
        self.win.touchwin()
        self.win.refresh()
    
    def listRooms(self):
        """Show rooms list"""
        userListWindow = curses.newwin(self.height-2, self.width, 1, 0)

        status = ["hidden", "active"]
        color = [0b111, 0b1010]
        maxW = 0
        xOffset = 1
        indexOffset = 0

        for index, roomName in enumerate(self.data.log.keys()):
            if (index-indexOffset)+1 == self.height-4: #off=0 idx=9 h=10
                indexOffset += self.height-4
                xOffset += maxW + 1
                maxW = 0
            else:
                txt = "[%s] %s" % (status[roomName in self.data.rooms], roomName)
                maxW = max(maxW, len(txt))
                userListWindow.insstr(index - indexOffset + 1, xOffset, txt, curses.color_pair(color[roomName in self.data.rooms]))

        userListWindow.border(0)
        userListWindow.refresh()

        userListWindow.get_wch() # Wait a key press
        del userListWindow
        self.win.touchwin()
        self.win.refresh()

    def getTextLine(self):
        """Allow the user to insert a line into the chat log"""
        curses.echo()
        curses.curs_set(1)

        self.drawInputWin()
        txt = self.screen.getstr()

        curses.noecho()
        if len(txt)>0:
            self.data.appendLog(txt.decode("utf8", errors="ignore"))
            self.data.curUserId, self.data.lastUserId = self.data.lastUserId, self.data.curUserId

            # Increment offset to look at end of log when you input a new line
            logSize = self.data.getLogSize()
            self.offset = max(0, logSize-(self.height-4))

    def newRoom(self):
        """Adds a room"""
        curses.echo()
        curses.curs_set(1)
        
        self.screen.addstr(self.height-1, 0, "[room name] ")
        self.screen.clrtoeol()
        self.screen.refresh()

        txt = ""
        txt = self.screen.getstr().decode("utf8", errors="ignore")

        if len(txt)>0:
            self.data.rooms.append(txt)
            if not txt in self.data.log:
                self.data.log[txt] = []

            self.data.room = len(self.data.rooms) - 1
            self.offset = max(0, self.data.getLogSize() - (self.height-4))
            self.__initWinLayouts__()
        curses.noecho()
        curses.curs_set(0)

    def importExport(self, arg="export"):
        """Import from / Export to a log text file
        arg: 'export' → send to file, 'import' → import from file"""
        curses.echo()
        curses.curs_set(1)
        
        self.screen.addstr(self.height-1, 0, "[{} file name] ".format(arg))
        self.screen.clrtoeol()
        self.screen.refresh()

        txt = ""
        txt = self.screen.getstr(self.width-12).decode("utf8", errors="ignore")

        if len(txt)>0:
            # Export
            if arg=="export":
                self.data.exportLog(txt)
            # Import
            elif arg=="import":
                self.data.importLog(txt)

        curses.noecho()

# vim: ts=4 sts=4 sw=4 et
