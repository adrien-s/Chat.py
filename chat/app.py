#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""CursesChat Application"""

import curses
import os
import pickle

from .data import Data
from .win import Win

class App:
    """Chat app instance
    cData: Data"""

    def __init__(self, data = Data()):
        """Inits cData"""
        self.cData = data
        self.filepath = "data.pickle"
        self.cmdHooks = {
            "i": self.__cmdInsert, "\x0A": self.__cmdInsert,
            "KEY_UP": self.__cmdCursor, "KEY_DOWN": self.__cmdCursor, "KEY_LEFT": self.__cmdCursor, "KEY_RIGHT": self.__cmdCursor, "KEY_PPAGE": self.__cmdCursor, "KEY_NPAGE": self.__cmdCursor, "KEY_HOME": self.__cmdCursor, "^": self.__cmdCursor, "KEY_END": self.__cmdCursor, "G": self.__cmdCursor,
            "	": self.__cmdSwitchRoom, "KEY_BTAB": self.__cmdSwitchRoom,
            "D": self.__cmdRoomDel, "c": self.__cmdRoomDel,
            "d": self.__cmdDelLog,
            "e": self.__cmdImportExport, "w": self.__cmdImportExport,
            "n": self.__cmdData, "+": self.__cmdData,
            "p": self.__cmdData, "-": self.__cmdData,
            "r": self.__cmdData,
            "h": self.__cmdData,
            "l": self.__cmdData
        }

    def load(self, pickleFilePath=""):
        """Load app from a pickle file"""
        if pickleFilePath == "":
           pickleFilePath = self.filepath 
        if not os.path.isfile(pickleFilePath):
            print("ERR: %s not found" % pickleFilePath)
            return

        pickleFile = open(pickleFilePath, "rb")
        self.filepath = pickleFilePath
        data = pickle.load(pickleFile)
        
        self.cData = Data()

        self.cData.users       = data["users"]
        self.cData.curUserId   = data["curUserId"]
        self.cData.lastUserId  = data["lastUserId"]
        self.cData.log         = data["log"]
        self.cData.room        = data["curRoom"]
        self.cData.rooms       = data["roomList"]

        pickleFile.close()
    
    def save(self, pickleFilePath=""):
        """Save app to pickle file"""
        if pickleFilePath == "":
           pickleFilePath = self.filepath 
        
        pickleFile = open(pickleFilePath, "wb")
        pickle.dump({"users": self.cData.users, "curUserId": self.cData.curUserId, "lastUserId": self.cData.lastUserId, "log": self.cData.log, "roomList": self.cData.rooms, "curRoom": self.cData.room}, pickleFile)
        pickleFile.close()

    def initData(self, userList=None):
        """Creates a new empty chat data storage"""
        if userList != None:
            self.cData = Data(userList)
        else:
            self.cData = Data()

    def start(self):
        """Starts the main app loop"""
        if self.cData==None:
            self.initData()

        self.cWin = Win(self.cData)

        chStack = ""
        self.cWin.drawWindow()

        while True:
            curses.curs_set(0)
            try:
                curCh = self.cWin.screen.getkey()
            except curses.error:
                curCh = ""
                if curses.is_term_resized(self.cWin.height, self.cWin.width):
                    self.cWin.__resizeTerm__()

            # Escape → cancel command stack
            if curCh=="\x1B":
                chStack = ""
            # Exit chat
            elif curCh=='q':
                break
            # Bound character: executes associated function(s)
            elif curCh in self.cmdHooks:
                self.cmdHooks[curCh](curCh, chStack)
                self.cWin.drawWindow()
                chStack = ""
            # Print current command stack and add current character
            else:
                chStack += curCh

            # Draw command bar
            try:
                self.cWin.screen.addstr(self.cWin.height-1, 0, "<"+str(self.cData.curUserId)+":")
                self.cWin.screen.addstr(self.cData.users[self.cData.curUserId][0], curses.color_pair(self.cData.users[self.cData.curUserId][1]))
                self.cWin.screen.addstr("> ")
                self.cWin.screen.addstr(chStack.encode("latin1").decode("utf8"), curses.color_pair(0b0111))
                self.cWin.screen.clrtoeol()
            except UnicodeDecodeError:
                pass    
            self.cWin.screen.refresh()
            self.cWin.win.refresh()

        # Clears curses windows at exit
        self.cWin.__clear__()
        self.save()
            

    ##### Internal main commands #####
    def __cmdInsert(self, cur, argStr=""):
        """Insert mode"""
        userId = self.cData.curUserId
        try:    # If number passed before, take it as user number to switch to
            userId = int(argStr)
        except ValueError:
            pass
        finally:
            self.cData.curUserId = userId % len(self.cData.users)

        self.cWin.getTextLine()
        self.save()

    def __cmdCursor(self, cur, argStr=""):
        """Cursor movement"""

        logSize = self.cData.getLogSize()
        
        # Horizontal ⇐
        if cur=="KEY_LEFT" and self.cWin.hOffset>=int(self.cWin.width/4):
            self.cWin.hOffset -= int(self.cWin.width/4)
        # Horizontal ⇒
        elif cur=="KEY_RIGHT":
            self.cWin.hOffset += int(self.cWin.width/4)
        # Line ⇑
        elif cur=="KEY_UP" and self.cWin.offset>0:
            self.cWin.offset -= 1
        # Line ⇓
        elif cur=="KEY_DOWN" and (self.cWin.offset+self.cWin.height-4<logSize):
            self.cWin.offset += 1
        # Page ⇑
        elif cur=="KEY_PPAGE":
            self.cWin.offset = max(0, self.cWin.offset - (self.cWin.height-4))
        # Page ⇓
        elif cur=="KEY_NPAGE":
            if self.cWin.offset + self.cWin.height-4 < logSize:
                self.cWin.offset += self.cWin.height-4
                if self.cWin.offset + self.cWin.height-4 > logSize:
                    self.cWin.offset = logSize - (self.cWin.height-4)
        # Top of buffer
        elif cur in ["^", "KEY_HOME"]:
            self.cWin.offset = 0
        # Bottom of buffer
        elif cur == "KEY_END":
            self.cWin.offset = logSize - (self.cWin.height-4)
        # Sets buffer display offset
        elif cur=="G":
            lnOffset = max(0,logSize - (self.cWin.height-4))
            try:
                lnOffset = int(argStr)
            except ValueError:
                pass
            finally:
                self.cWin.offset = lnOffset
                if (self.cWin.offset + self.cWin.height-4) > logSize:
                    self.cWin.offset = logSize - (self.cWin.height-4)
    
        # Offset can't be negative
        self.cWin.offset = max(0, self.cWin.offset)

    def __cmdSwitchRoom(self, cur, argStr=""):
        """Switch between rooms"""
        # Switch to next room
        if cur=="	":
            roomId = self.cData.room + 1
            try:
                roomId = int(argStr)
            except ValueError:
                pass
            finally:
                self.cData.room = (roomId) % len(self.cData.rooms)
                self.cWin.offset = max(0, self.cData.getLogSize() - (self.cWin.height-4))
        # Switch to previous room
        elif cur=="KEY_BTAB":
            self.cData.prevRoom()
            self.cWin.__initWinLayouts__() 
    
    def __cmdRoomDel(self, cur, argStr=""):
        """Deletes current room if not the only one"""
        if len(self.cData.rooms)>1:
            if cur=="D":
                del self.cData.log[self.cData.rooms[self.cData.room]]

            del self.cData.rooms[self.cData.room]
            self.cData.room = max(0, self.cData.room - 1)
            self.cWin.offset = max(0, self.cData.getLogSize() - (self.cWin.height-4))
                
            self.cWin.__initWinLayouts__()

    def __cmdDelLog(self, cur, argStr=""):
        """Delete a log line"""
        logSize = self.cData.getLogSize()

        lnDel = logSize - 1
        if logSize > 0:
            try:
                lnDel = min(max(int(argStr),0), logSize)
            except ValueError:
                pass
            finally:
                self.cWin.offset = max(0, logSize - (self.cWin.height-4))
                del self.cData.log[self.cData.rooms[self.cData.room]][lnDel]
                self.cWin.win.touchwin()
               
    def __cmdImportExport(self, cur, argStr=""):
        """Import / Export log file"""
        # e=Edit, w=Write
        self.cWin.importExport({"e":"import", "w":"export"}[cur])
        if cur=="e":
              self.cWin.__initWinLayouts__()
    
    def __cmdData(self, cur, argStr):
        """Commands against Chat.Data"""
        if cur in ["n", "+"]:
            self.cData.nextUser()
        elif cur in ["p", "-"]:
            self.cData.prevUser()
        elif cur == "r":
            self.cWin.newRoom()
        elif cur == "h":
            self.cWin.showHelp()
        elif cur == "l":
            self.cWin.listRooms()

# vim: ts=4 sts=4 sw=4 et
