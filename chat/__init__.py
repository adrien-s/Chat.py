#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""CursesChat v0.3
A simple local chat system using Ncurses"""

from .app import App

def run(*args):
    """Run CursesChat against a defined file.
    """
    chat = App()
    if len(args) > 0:
        chat.load(args[0])
    chat.start()

def runcli():
    import sys
    run(*sys.argv[1:])

# vim: ts=4 sts=4 sw=4 et
